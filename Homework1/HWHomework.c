//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>
#include "string.h"
/*
 * Задание 1.
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns) {
    long **matrix=(long**)calloc(rows,sizeof(*matrix));
    for(int i=0;i<rows;i++){
        matrix[i] = (long*)calloc(columns, sizeof(*matrix[i]));
    }
    for(int i=0;i<rows;i++){
        for(int j=0;j<columns;j++){
            matrix[i][j]=rand()%200-100;
        }
    }
    return matrix;
}

/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности:
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности:
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString) {
    char *str[]={"ноль ","один ","два ","три ","четыре ","пять ","шесть ","семь ","восемь ","девять "};
    char *newString=(char*)calloc(100,sizeof(newString));
    for(int i=0;i<strlen(numberString);i++){
        
        if(numberString[i]=='.'){
            strcat(newString,"точка ");
        }else{
            int x=numberString[i]-'0';
            strcat(newString,str[x]);
        }
    }
    return newString;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность: 
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности:
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности:
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с прмерами. 
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */

float r=0;

HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    if(!r&&time<1){
        r=canvasSize/2.0;
    }else{
        r-=0.5;
    }
    if(r<(canvasSize/2.0)*(-1)){
        r=canvasSize/2.0;
    }
    float speed=6.4;
    float x,y,x0,y0;
    x0=y0=canvasSize/2.0;
    x=x0+r*cos(-time*speed);
    y=y0+r*sin(-time*speed);
    
    return (HWPoint){x, y};
}

